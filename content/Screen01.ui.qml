

/*
This is a UI file (.ui.qml) that is intended to be edited in Qt Design Studio only.
It is supposed to be strictly declarative and only uses a subset of QML. If you edit
this file manually, you might introduce QML code that is not supported by Qt Design Studio.
Check out https://doc.qt.io/qtcreator/creator-quick-ui-forms.html for details on .ui.qml files.
*/
import QtQuick 6.2
import QtQuick.Controls 6.2
import ToDo

Rectangle {
    id: rectangle
    width: Constants.width
    height: Constants.height
    color: "#3b59b0"

    Text {
        id: text2
        height: 33
        text: qsTr("To Do Today")
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        font.pixelSize: 24
        horizontalAlignment: Text.AlignHCenter
        anchors.rightMargin: 10
        anchors.leftMargin: 10
        anchors.topMargin: 10
    }

    Button {
        id: addToDobutton
        y: 758
        text: qsTr("Add To-do")
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        padding: 4
        icon.color: "#db6161"
        anchors.rightMargin: 95
        anchors.leftMargin: 95
        anchors.bottomMargin: 274
    }

    Rectangle {
        id: rectangle1
        x: 8
        y: 854
        width: 392
        height: 200
        color: "#ffffff"
        radius: 20

        Text {
            id: text1
            height: 59
            text: qsTr("Text")
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            font.pixelSize: 12
            anchors.rightMargin: 14
            anchors.leftMargin: 25
            anchors.topMargin: 25

            Row {
                x: 90
                anchors.verticalCenter: parent.verticalCenter
                spacing: 50
                Button {
                    id: addButton
                    text: qsTr("Add")
                }

                Button {
                    id: cancelButton
                    text: qsTr("Cancel")
                }
            }
        }
    }

    Rectangle {
        id: toDoItem
        x: 8
        y: 16
        width: 384
        height: 200
        color: "#36ed9e"

        CheckBox {
            id: checkBox
            text: qsTr("Check Box")
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.leftMargin: 0
            anchors.bottomMargin: 75
            anchors.topMargin: 75
        }
    }

    states: [
        State {
            name: "clicked"
            when: addToDobutton.checked
        }
    ]
}

/*##^##
Designer {
    D{i:0}D{i:10;annotation:"1 //;;//  //;;//  //;;// <!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\nhr { height: 1px; border-width: 0; }\nli.unchecked::marker { content: \"\\2610\"; }\nli.checked::marker { content: \"\\2612\"; }\n</style></head><body style=\" font-family:'Noto Sans'; font-size:9pt; font-weight:400; font-style:normal;\">\n<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html> //;;// 1672328051";customId:"Add To-do"}
D{i:5}D{i:4}
}
##^##*/

